import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import { addRxPlugin } from 'rxdb';
import { RxDBDevModePlugin } from 'rxdb/plugins/dev-mode';
import { environment } from './environments/environment';

if (!environment.production) {
  addRxPlugin(RxDBDevModePlugin);
}

bootstrapApplication(AppComponent, appConfig).catch((err) =>
  console.error(err),
);
