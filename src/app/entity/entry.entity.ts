import { DistanceModel } from '../model/distance.model';
import { EntryModel } from '../model/entry.model';
import { SavableEntity } from './savable.entity';

export abstract class EntryEntity extends SavableEntity implements EntryModel {
  public readonly name: string;
  public readonly toDistances: DistanceModel[];

  protected constructor(props: EntryModel) {
    super(props);

    this.name = props.name;
    this.toDistances = props.toDistances;
  }
}
