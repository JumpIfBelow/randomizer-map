import { BaseEntity } from './base.entity';
import { StateModel } from '../model/state.model';
import { AreaModel } from '../model/area.model';
import { CapabilityModel } from '../model/capability.model';
import { CheckModel } from '../model/check.model';
import { CheckTypeModel } from '../model/check-type.model';
import { DistanceModel } from '../model/distance.model';
import { EntranceModel } from '../model/entrance.model';
import { SpawnModel } from '../model/spawn.model';
import { WarpModel } from '../model/warp.model';

export abstract class StateEntity extends BaseEntity implements StateModel {
  public name: string;
  public readonly areas: AreaModel[];
  public readonly capabilities: CapabilityModel[];
  public readonly checks: CheckModel[];
  public readonly checkTypes: CheckTypeModel[];
  public readonly distances: DistanceModel[];
  public readonly entrances: EntranceModel[];
  public readonly spawns: SpawnModel[];
  public readonly warps: WarpModel[];

  constructor(props: StateModel) {
    super(props);

    this.name = props.name;
    this.areas = props.areas;
    this.capabilities = props.capabilities;
    this.checks = props.checks;
    this.checkTypes = props.checkTypes;
    this.distances = props.distances;
    this.entrances = props.entrances;
    this.spawns = props.spawns;
    this.warps = props.warps;
  }
}
