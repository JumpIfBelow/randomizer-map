import { CheckModel } from '../model/check.model';
import { CheckTypeModel } from '../model/check-type.model';
import { AreaModel } from '../model/area.model';
import { CapabilityModel } from '../model/capability.model';
import { SavableEntity } from './savable.entity';
import { RequirementModel } from '../model/requirement.model';

export class CheckEntity extends SavableEntity implements CheckModel {
  public readonly name: string;
  public readonly checkType: CheckTypeModel;
  public readonly from: AreaModel;
  public isDone: boolean;
  public readonly requirements: RequirementModel[];
  public readonly rewards: CapabilityModel[];

  constructor(props: CheckModel) {
    super(props);

    this.name = props.name;
    this.checkType = props.checkType;
    this.from = props.from;
    this.isDone = props.isDone;
    this.requirements = props.requirements;
    this.rewards = props.rewards;
  }

  public do(): this {
    this.isDone = true;

    return this;
  }

  public undo(): this {
    this.isDone = false;

    return this;
  }

  public toggle(): this {
    this.isDone = !this.isDone;

    return this;
  }
}
