import { RequirementModel } from '../model/requirement.model';
import { CheckModel } from '../model/check.model';
import { CapabilityModel } from '../model/capability.model';
import { SavableEntity } from './savable.entity';

export class RequirementEntity
  extends SavableEntity
  implements RequirementModel
{
  readonly check: CheckModel;
  readonly capabilities: CapabilityModel[];

  constructor(props: RequirementModel) {
    super(props);

    this.check = props.check;
    this.capabilities = props.capabilities;
  }
}
