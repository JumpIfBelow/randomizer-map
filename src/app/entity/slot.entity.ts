import { SlotModel } from '../model/slot.model';
import { AreaModel } from '../model/area.model';
import { StateEntity } from './state.entity';

export class SlotEntity extends StateEntity implements SlotModel {
  public addArea(area: AreaModel): void {
    this.areas.push(area);
  }

  public removeArea(area: AreaModel): void {
    const index = this.areas.indexOf(area);
    if (index > -1) {
      this.areas.splice(index, 1);
    }
  }
}
