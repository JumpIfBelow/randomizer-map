import { WarpModel } from '../model/warp.model';
import { SpawnModel } from '../model/spawn.model';
import { SavableEntity } from './savable.entity';

export class WarpEntity extends SavableEntity implements WarpModel {
  public readonly name: string;
  public to?: SpawnModel;

  constructor(props: WarpModel) {
    super(props);

    this.name = props.name;
    this.to = props.to;
  }
}
