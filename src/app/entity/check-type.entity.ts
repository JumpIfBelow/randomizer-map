import { CheckTypeModel } from '../model/check-type.model';
import { CheckModel } from '../model/check.model';
import { SavableEntity } from './savable.entity';

export class CheckTypeEntity extends SavableEntity implements CheckTypeModel {
  public readonly name: string;
  public readonly checks: CheckModel[];

  constructor(props: CheckTypeModel) {
    super(props);

    this.name = props.name;
    this.checks = props.checks;
  }
}

export const CheckTypeEnum = Object.freeze({
  CHEST: new CheckTypeEntity({
    name: 'Chest',
    checks: [],
    templates: [],
    slots: [],
  }),
  COW: new CheckTypeEntity({
    name: 'Cow',
    checks: [],
    templates: [],
    slots: [],
  }),
  NPC: new CheckTypeEntity({
    name: 'NPC',
    checks: [],
    templates: [],
    slots: [],
  }),
  SCRUB: new CheckTypeEntity({
    name: 'Scrub',
    checks: [],
    templates: [],
    slots: [],
  }),
  SHOP: new CheckTypeEntity({
    name: 'Shop',
    checks: [],
    templates: [],
    slots: [],
  }),
  SKULLTULA: new CheckTypeEntity({
    name: 'Skulltula',
    checks: [],
    templates: [],
    slots: [],
  }),
  SONG: new CheckTypeEntity({
    name: 'Song',
    checks: [],
    templates: [],
    slots: [],
  }),
});
