import { BaseModel } from '../model/base.model';

export abstract class BaseEntity implements BaseModel {
  public id?: number;

  protected constructor(props: BaseModel) {
    this.id = props.id;
  }
}
