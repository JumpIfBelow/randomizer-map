import { TemplateModel } from '../model/template.model';
import { StateEntity } from './state.entity';

export class TemplateEntity extends StateEntity implements TemplateModel {}
