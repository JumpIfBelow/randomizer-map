import { EntranceModel } from '../model/entrance.model';
import { CheckTypeModel } from '../model/check-type.model';
import { CheckModel } from '../model/check.model';
import { AreaModel } from '../model/area.model';
import { EntranceEntity } from './entrance.entity';
import { CheckEntity } from './check.entity';
import { CapabilityModel } from '../model/capability.model';
import { SavableEntity } from './savable.entity';

export class AreaEntity extends SavableEntity implements AreaModel {
  public readonly name: string;
  public readonly entrances: EntranceModel[];
  public readonly checks: CheckModel[];

  constructor(props: AreaModel) {
    super(props);

    this.name = props.name;
    this.entrances = props.entrances;
    this.checks = props.checks;
  }

  public addEntrance(name: string): this {
    this.entrances.push(
      new EntranceEntity({
        name,
        from: this,
        fromDistances: [],
        toDistances: [],
        templates: [],
        slots: [],
      }),
    );

    return this;
  }

  public addCheck(
    name: string,
    checkType: CheckTypeModel,
    requirements: CapabilityModel[],
    rewards: CapabilityModel[],
  ): this {
    this.checks.push(
      new CheckEntity({
        name,
        checkType,
        from: this,
        isDone: false,
        requirements,
        rewards,
        templates: [],
        slots: [],
      }),
    );

    return this;
  }

  public findEntrance(name: string): EntranceModel | undefined {
    return this.entrances.find((link) => link.name === name);
  }

  public findCheck(name: string): CheckModel | undefined {
    return this.checks.find((check) => check.name === name);
  }
}
