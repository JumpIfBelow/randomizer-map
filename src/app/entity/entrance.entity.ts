import { EntranceModel } from '../model/entrance.model';
import { AreaModel } from '../model/area.model';
import { EntryEntity } from './entry.entity';
import { DistanceModel } from '../model/distance.model';

export class EntranceEntity extends EntryEntity implements EntranceModel {
  public readonly from: AreaModel;
  public to?: EntranceModel;
  public readonly fromDistances: DistanceModel[];

  constructor(props: EntranceModel) {
    super(props);

    this.from = props.from;
    this.to = props.to;
    this.fromDistances = props.fromDistances;
  }
}
