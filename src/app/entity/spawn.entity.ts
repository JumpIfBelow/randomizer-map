import { SpawnModel } from '../model/spawn.model';
import { AreaModel } from '../model/area.model';
import { WarpModel } from '../model/warp.model';
import { EntryEntity } from './entry.entity';

export class SpawnEntity extends EntryEntity implements SpawnModel {
  public readonly to: AreaModel;
  public from?: WarpModel;

  constructor(props: SpawnModel) {
    super(props);

    this.to = props.to;
    this.from = props.from;
  }
}
