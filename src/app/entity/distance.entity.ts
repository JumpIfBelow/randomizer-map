import { DistanceModel } from '../model/distance.model';
import { EntryModel } from '../model/entry.model';
import { SavableEntity } from './savable.entity';

export class DistanceEntity extends SavableEntity implements DistanceModel {
  public readonly from: EntryModel;
  public readonly to: EntryModel;
  public readonly weight: number;

  constructor(props: DistanceModel) {
    super(props);

    this.from = props.from;
    this.to = props.to;
    this.weight = props.weight;
  }
}
