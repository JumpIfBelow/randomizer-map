import { BaseEntity } from './base.entity';
import { SavableModel } from '../model/savable.model';
import { SlotModel } from '../model/slot.model';
import { TemplateModel } from '../model/template.model';

export abstract class SavableEntity extends BaseEntity implements SavableModel {
  readonly templates: readonly TemplateModel[];
  readonly slots: SlotModel[];

  protected constructor(props: SavableModel) {
    super(props);

    this.templates = props.templates;
    this.slots = props.slots;
  }
}
