import { CapabilityModel } from '../model/capability.model';
import { CheckModel } from '../model/check.model';
import { SavableEntity } from './savable.entity';
import { RequirementModel } from '../model/requirement.model';

export class CapabilityEntity extends SavableEntity implements CapabilityModel {
  public readonly name: string;
  public isOwned: boolean;
  public readonly requirements: RequirementModel[];
  public readonly rewarders: CheckModel[];

  constructor(props: CapabilityModel) {
    super(props);

    this.name = props.name;
    this.isOwned = props.isOwned;
    this.requirements = props.requirements;
    this.rewarders = props.rewarders;
  }
}
