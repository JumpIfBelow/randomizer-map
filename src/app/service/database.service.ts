import { createRxDatabase } from 'rxdb/dist/types/rx-database';
import { getRxStorageDexie } from 'rxdb/dist/types/plugins/storage-dexie/rx-storage-dexie';
import { AreaSchema } from '../schema/area.schema';
import { WarpSchema } from '../schema/warp.schema';
import { TemplateSchema } from '../schema/template.schema';
import { SpawnSchema } from '../schema/spawn.schema';
import { SlotSchema } from '../schema/slot.schema';
import { EntranceSchema } from '../schema/entrance.schema';
import { DistanceSchema } from '../schema/distance.schema';
import { CheckTypeSchema } from '../schema/check-type.schema';
import { CheckSchema } from '../schema/check.schema';
import { CapabilitySchema } from '../schema/capability.schema';
import { inject, Injectable, Injector, Signal, untracked } from '@angular/core';
import { RxReactivityFactory } from 'rxdb/dist/types/types';
import { toSignal } from '@angular/core/rxjs-interop';
import { RequirementSchema } from '../schema/requirement.schema';

function createReactivityFactory(
  injector: Injector,
): RxReactivityFactory<Signal<any>> {
  return {
    fromObservable(observable$, initialValue: any) {
      return untracked(() =>
        toSignal(observable$, {
          initialValue,
          injector,
          rejectErrors: true,
        }),
      );
    },
  };
}

const database = await createRxDatabase({
  name: 'randomizer-map',
  // storage: getRxStorageIndexedDB(),  // FIXME check for a paid version if ever available
  storage: getRxStorageDexie(),
  eventReduce: true,
  reactivity: createReactivityFactory(inject(Injector)),
});

const collections = await database.addCollections({
  area: AreaSchema,
  capability: CapabilitySchema,
  check: CheckSchema,
  checkType: CheckTypeSchema,
  distance: DistanceSchema,
  entrance: EntranceSchema,
  requirement: RequirementSchema,
  slot: SlotSchema,
  spawn: SpawnSchema,
  template: TemplateSchema,
  warp: WarpSchema,
});

@Injectable({
  providedIn: 'root',
})
export class DatabaseService {
  public get database(): typeof database {
    return database;
  }

  public get collections(): typeof collections {
    return collections;
  }
}
