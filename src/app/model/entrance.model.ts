import { AreaModel } from './area.model';
import { DistanceModel } from './distance.model';
import { EntryModel } from './entry.model';

export interface EntranceModel extends EntryModel {
  readonly from: AreaModel;
  to?: EntranceModel;
  readonly fromDistances: DistanceModel[];
}
