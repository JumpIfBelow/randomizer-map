import { CheckModel } from './check.model';
import { SavableModel } from './savable.model';
import { RequirementModel } from './requirement.model';

export interface CapabilityModel extends SavableModel {
  readonly name: string;
  isOwned: boolean;
  readonly requirements: RequirementModel[];
  readonly rewarders: CheckModel[];
}
