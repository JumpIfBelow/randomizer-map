import { CheckModel } from './check.model';
import { SavableModel } from './savable.model';

export interface CheckTypeModel extends SavableModel {
  readonly name: string;
  readonly checks: CheckModel[];
}
