import { DistanceModel } from './distance.model';
import { SavableModel } from './savable.model';

export interface EntryModel extends SavableModel {
  readonly name: string;
  readonly toDistances: DistanceModel[];
}
