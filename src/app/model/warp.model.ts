import { SpawnModel } from './spawn.model';
import { SavableModel } from './savable.model';

export interface WarpModel extends SavableModel {
  readonly name: string;
  to?: SpawnModel;
}
