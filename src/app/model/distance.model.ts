import { EntryModel } from './entry.model';
import { SavableModel } from './savable.model';

export interface DistanceModel extends SavableModel {
  readonly from: EntryModel;
  readonly to: EntryModel;
  readonly weight: number;
}
