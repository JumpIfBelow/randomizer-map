import { SavableModel } from './savable.model';
import { CapabilityModel } from './capability.model';
import { CheckModel } from './check.model';

export interface RequirementModel extends SavableModel {
  readonly check: CheckModel;
  readonly capabilities: CapabilityModel[];
}
