import { CheckModel } from './check.model';
import { EntranceModel } from './entrance.model';
import { SavableModel } from './savable.model';

export interface AreaModel extends SavableModel {
  readonly name: string;
  readonly entrances: EntranceModel[];
  readonly checks: CheckModel[];
}
