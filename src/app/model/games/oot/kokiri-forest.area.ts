import { AreaModel } from '../../area.model';
import { CheckTypeEnum } from '../../check-type.model';

const area = new AreaModel('Kokiri Forest');

area
  .addEntrance("Link's House")
  .addEntrance('Lost Woods')
  .addEntrance("Near Lost Wood Kokiri Forest's Grotto")
  .addEntrance('Kokiri Shop')
  .addEntrance('Know-It-All Brothers House')
  .addEntrance('Saria House')
  .addEntrance('Mido House')
  .addEntrance('Inside the Deku Tree')
  .addEntrance("Lost Woods' Bridge");

area.addCheck('Kokiri Sword Chest', CheckTypeEnum.CHEST);

export default area;
