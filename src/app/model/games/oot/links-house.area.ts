import { AreaModel } from '../../area.model';
import { CheckTypeEnum } from '../../check-type.model';

const area = new AreaModel("Link's House");

area.addEntrance('Kokiri Forest');

area.addCheck('Cow', CheckTypeEnum.COW);

export default area;
