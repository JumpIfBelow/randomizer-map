import { AreaModel } from '../../area.model';

const area = new AreaModel('Secret Forest Meadow');

area.addEntrance('Lost Woods');

export default area;
