import { SlotModel } from './slot.model';
import { TemplateModel } from './template.model';
import { BaseModel } from './base.model';

export interface SavableModel extends BaseModel {
  readonly templates: readonly TemplateModel[];
  readonly slots: SlotModel[];
}
