import { BaseModel } from './base.model';
import { AreaModel } from './area.model';
import { CheckTypeModel } from './check-type.model';
import { CapabilityModel } from './capability.model';
import { WarpModel } from './warp.model';
import { SpawnModel } from './spawn.model';
import { EntranceModel } from './entrance.model';
import { DistanceModel } from './distance.model';
import { CheckModel } from './check.model';
import { RequirementModel } from './requirement.model';

export interface StateModel extends BaseModel {
  readonly name: string;
  readonly areas: AreaModel[];
  readonly capabilities: CapabilityModel[];
  readonly checks: CheckModel[];
  readonly checkTypes: CheckTypeModel[];
  readonly distances: DistanceModel[];
  readonly entrances: EntranceModel[];
  readonly requirements: RequirementModel[];
  readonly spawns: SpawnModel[];
  readonly warps: WarpModel[];
}
