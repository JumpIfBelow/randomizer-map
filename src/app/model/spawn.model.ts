import { AreaModel } from './area.model';
import { WarpModel } from './warp.model';
import { EntryModel } from './entry.model';

export interface SpawnModel extends EntryModel {
  readonly to: AreaModel;
  from?: WarpModel;
}
