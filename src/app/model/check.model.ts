import { AreaModel } from './area.model';
import { CheckTypeModel } from './check-type.model';
import { CapabilityModel } from './capability.model';
import { SavableModel } from './savable.model';
import { RequirementModel } from './requirement.model';

export interface CheckModel extends SavableModel {
  readonly name: string;
  readonly checkType: CheckTypeModel;
  readonly from: AreaModel;
  isDone: boolean;
  readonly requirements: RequirementModel[];
  readonly rewards: CapabilityModel[];
}
