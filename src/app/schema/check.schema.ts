import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { CheckModel } from '../model/check.model';

export const CheckSchema: RxCollectionCreator<CheckModel> = {
  schema: {
    title: 'check',
    version: 0,
    description:
      'Check is a any interaction with an NPC, a chest or anything else that can give a new capability to advance in the game.',
    primaryKey: 'id',
    type: 'object',
    required: [
      'id',
      'name',
      'checkType',
      'from',
      'isDone',
      'requirements',
      'rewards',
      'templates',
      'slots',
    ],
    properties: {
      id: {
        type: 'number',
      },
      name: {
        type: 'string',
        maxLength: 255,
        uniqueItems: true,
      },
      checkType: {
        type: 'number',
        ref: 'checkType',
      },
      from: {
        type: 'number',
        ref: 'area',
      },
      isDone: {
        type: 'boolean',
      },
      requirements: {
        type: 'array',
        ref: 'requirement',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      rewards: {
        type: 'array',
        ref: 'requirement',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      templates: {
        type: 'array',
        ref: 'template',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      slots: {
        type: 'array',
        ref: 'slot',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
