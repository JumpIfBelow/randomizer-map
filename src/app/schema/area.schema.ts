import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { AreaModel } from '../model/area.model';

export const AreaSchema: RxCollectionCreator<AreaModel> = {
  schema: {
    title: 'area',
    version: 0,
    description: 'Area is a a whole zone of the map',
    primaryKey: 'id',
    type: 'object',
    required: ['id', 'name', 'checks', 'entrances', 'templates', 'slots'],
    properties: {
      id: {
        type: 'number',
      },
      name: {
        type: 'string',
        maxLength: 255,
        uniqueItems: true,
      },
      checks: {
        type: 'array',
        ref: 'check',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      entrances: {
        type: 'array',
        ref: 'entrance',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      templates: {
        type: 'array',
        ref: 'template',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      slots: {
        type: 'array',
        ref: 'slot',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
