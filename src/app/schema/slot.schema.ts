import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { SlotModel } from '../model/slot.model';

export const SlotSchema: RxCollectionCreator<SlotModel> = {
  schema: {
    title: 'slot',
    version: 0,
    description: 'Slot is a save point for a whole map.',
    primaryKey: 'id',
    type: 'object',
    required: [
      'id',
      'name',
      'areas',
      'capabilities',
      'checks',
      'checkTypes',
      'distances',
      'entrances',
      'spawns',
      'warps',
    ],
    properties: {
      id: {
        type: 'number',
      },
      name: {
        type: 'string',
        maxLength: 255,
        uniqueItems: true,
      },
      areas: {
        type: 'array',
        ref: 'area',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      capabilities: {
        type: 'array',
        ref: 'capability',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      checks: {
        type: 'array',
        ref: 'check',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      checkTypes: {
        type: 'array',
        ref: 'checkType',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      distances: {
        type: 'array',
        ref: 'distance',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      entrances: {
        type: 'array',
        ref: 'entrance',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      spawns: {
        type: 'array',
        ref: 'spawn',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      warps: {
        type: 'array',
        ref: 'warp',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
