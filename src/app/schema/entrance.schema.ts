import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { EntranceModel } from '../model/entrance.model';

export const EntranceSchema: RxCollectionCreator<EntranceModel> = {
  schema: {
    title: 'entrance',
    version: 0,
    description: 'Entrance is a trigger that connects two areas.',
    primaryKey: 'id',
    type: 'object',
    required: [
      'id',
      'name',
      'from',
      'fromDistances',
      'toDistances',
      'templates',
      'slots',
    ],
    properties: {
      id: {
        type: 'number',
      },
      name: {
        type: 'string',
        maxLength: 255,
        uniqueItems: true,
      },
      from: {
        type: 'number',
        ref: 'area',
      },
      to: {
        type: 'number',
        ref: 'area',
      },
      fromDistances: {
        type: 'array',
        ref: 'distance',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      toDistances: {
        type: 'array',
        ref: 'distance',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      templates: {
        type: 'array',
        ref: 'template',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      slots: {
        type: 'array',
        ref: 'slot',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
