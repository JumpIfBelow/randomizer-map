import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { SpawnModel } from '../model/spawn.model';

export const SpawnSchema: RxCollectionCreator<SpawnModel> = {
  schema: {
    title: 'spawn',
    version: 0,
    description:
      'Spawn is a point where a player can come from anywhere. It can be seen as an entry without a "from" entry.',
    primaryKey: 'id',
    type: 'object',
    required: ['id', 'name', 'toDistances', 'to', 'templates', 'slots'],
    properties: {
      id: {
        type: 'number',
      },
      name: {
        type: 'string',
        maxLength: 255,
        uniqueItems: true,
      },
      toDistances: {
        type: 'array',
        uniqueItems: true,
        ref: 'distance',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      to: {
        type: 'number',
        ref: 'area',
      },
      from: {
        type: 'number',
        ref: 'warp',
      },
      templates: {
        type: 'array',
        ref: 'template',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      slots: {
        type: 'array',
        ref: 'slot',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
