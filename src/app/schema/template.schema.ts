import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { TemplateModel } from '../model/template.model';

export const TemplateSchema: RxCollectionCreator<TemplateModel> = {
  schema: {
    title: 'template',
    version: 0,
    description:
      'Template is a save of a group of items, allowing to create a map from it.',
    primaryKey: 'id',
    type: 'object',
    required: [
      'id',
      'name',
      'areas',
      'capabilities',
      'checks',
      'checkTypes',
      'distances',
      'entrances',
      'spawns',
      'warps',
    ],
    properties: {
      id: {
        type: 'number',
      },
      name: {
        type: 'string',
        maxLength: 255,
        uniqueItems: true,
      },
      areas: {
        type: 'array',
        ref: 'area',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      capabilities: {
        type: 'array',
        ref: 'capability',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      checks: {
        type: 'array',
        ref: 'check',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      checkTypes: {
        type: 'array',
        ref: 'checkType',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      distances: {
        type: 'array',
        ref: 'distance',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      entrances: {
        type: 'array',
        ref: 'entrance',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      spawns: {
        type: 'array',
        ref: 'spawn',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      warps: {
        type: 'array',
        ref: 'warp',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
