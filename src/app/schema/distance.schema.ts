import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { DistanceModel } from '../model/distance.model';

export const DistanceSchema: RxCollectionCreator<DistanceModel> = {
  schema: {
    title: 'distance',
    version: 0,
    description:
      'Distance adds a weight between two entries in one direction. The other direction may or may not have the same weight.',
    primaryKey: 'id',
    type: 'object',
    required: ['id', 'from', 'to', 'weight', 'templates', 'slots'],
    properties: {
      id: {
        type: 'number',
      },
      from: {
        type: 'number',
        ref: 'entry',
      },
      to: {
        type: 'number',
        ref: 'entry',
      },
      weight: {
        type: 'number',
      },
      templates: {
        type: 'array',
        ref: 'template',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      slots: {
        type: 'array',
        ref: 'slot',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
