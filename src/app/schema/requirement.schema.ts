import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { RequirementModel } from '../model/requirement.model';

export const RequirementSchema: RxCollectionCreator<RequirementModel> = {
  schema: {
    title: 'requirement',
    version: 0,
    description:
      'Requirement links what a check requires to be available with which capabilities.',
    primaryKey: 'id',
    type: 'object',
    required: ['id', 'check', 'capabilities', 'templates', 'slots'],
    properties: {
      id: {
        type: 'number',
      },
      check: {
        type: 'number',
        ref: 'check',
        uniqueItems: true,
      },
      capabilities: {
        type: 'array',
        ref: 'capability',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      templates: {
        type: 'array',
        ref: 'template',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      slots: {
        type: 'array',
        ref: 'slot',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
