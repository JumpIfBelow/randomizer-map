import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { CapabilityModel } from '../model/capability.model';

export const CapabilitySchema: RxCollectionCreator<CapabilityModel> = {
  schema: {
    title: 'capability',
    version: 0,
    description:
      'Capability is an item, a trigger or any condition that allows to perform a check.',
    primaryKey: 'id',
    type: 'object',
    required: [
      'id',
      'name',
      'isOwned',
      'requirements',
      'rewarders',
      'templates',
      'slots',
    ],
    properties: {
      id: {
        type: 'number',
      },
      name: {
        type: 'string',
        maxLength: 255,
        uniqueItems: true,
      },
      isOwned: {
        type: 'boolean',
        default: false,
      },
      requirements: {
        type: 'array',
        ref: 'requirement',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      rewarders: {
        type: 'array',
        ref: 'check',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      templates: {
        type: 'array',
        ref: 'template',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      slots: {
        type: 'array',
        ref: 'slot',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
