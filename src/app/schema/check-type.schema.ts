import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { CheckTypeModel } from '../model/check-type.model';

export const CheckTypeSchema: RxCollectionCreator<CheckTypeModel> = {
  schema: {
    title: 'checkType',
    version: 0,
    description:
      'Check type describes a category of checks. It has no other purpose than searching and grouping checks.',
    primaryKey: 'id',
    type: 'object',
    required: ['id', 'name', 'checks', 'templates', 'slots'],
    properties: {
      id: {
        type: 'number',
      },
      name: {
        type: 'string',
        maxLength: 255,
        uniqueItems: true,
      },
      checks: {
        type: 'array',
        ref: 'check',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      templates: {
        type: 'array',
        ref: 'template',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      slots: {
        type: 'array',
        ref: 'slot',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
