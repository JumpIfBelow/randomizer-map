import { RxCollectionCreator } from 'rxdb/dist/types/types';
import { WarpModel } from '../model/warp.model';

export const WarpSchema: RxCollectionCreator<WarpModel> = {
  schema: {
    title: 'warp',
    version: 0,
    description:
      'Warp is a an entrance that can be triggered anywhere to a spawn point.',
    primaryKey: 'id',
    type: 'object',
    required: ['id', 'name', 'to', 'templates', 'slots'],
    properties: {
      id: {
        type: 'number',
      },
      name: {
        type: 'string',
        maxLength: 255,
        uniqueItems: true,
      },
      to: {
        type: 'number',
        ref: 'spawn',
      },
      templates: {
        type: 'array',
        ref: 'template',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
      slots: {
        type: 'array',
        ref: 'slot',
        items: {
          type: 'number',
          uniqueItems: true,
        },
      },
    },
  },
};
